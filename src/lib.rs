/*
   Copyright 2020 Bota Viorel

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

use mongodb::{Client};
#[macro_use]
extern crate bson;
extern crate mongodb;

use logger::log;
use bson::spec::BinarySubtype;

pub fn store(database_server: String, database_name: String, collection_name: String, raw_data: Vec<u8>) {
    #[cfg(debug_assertions)] {
        log::debug(format!("Database server address is {}", database_server))
    }
    let client = match Client::with_uri_str(&database_server) {
        Ok(client) => client,
        Err(error) => {
            let error_message = format!("Can not open data connection to the database server {}. Error {}", database_server, error);
            log::fatal(error_message.clone());
            panic!(error_message);
        }
    };

    #[cfg(debug_assertions)] {
        log::debug(format!("Database name is {}", database_name))
    }
    let database = client.database( database_name.as_str() );

    #[cfg(debug_assertions)] {
        log::debug(format!("Collection name is {}", collection_name))
    }
    let collection = database.collection( collection_name.as_str() );

    #[cfg(debug_assertions)] {
        log::debug(format!("Data to save is {:?}", raw_data))
    }
    match collection.insert_one(doc! { "raw_data": (BinarySubtype::Generic, raw_data) }, None ) {
        Ok(_) => {},
        Err(error) => {
            let error_message = format!("Failed to insert new record in database {} collection {} from server {}. Error {}", database_name, collection_name, database_server, error);
            log::fatal(error_message.clone());
            panic!(error_message);
        }
    };
}